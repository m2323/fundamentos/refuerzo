
from menu import header_table
from delete_screen import delete_screen

STUDENTS = []

ACADEMIC_PROGRAM_LIST = [
        {"id":1 , "name":"Sistemas", "cost":800000},
        {"id":2 , "name":"Desarrollo de videojuegos", "cost":1000000},
        {"id":3 , "name":"Animación Digital", "cost":1200000}
    ]

SCHOLARISHIP_LIST = [
        {"id":1, "type":"Rendimiento Académico", "discount":50},
        {"id":2, "type":"Cultural - Deportes", "discount":40},
        {"id":3, "type":"Sin Beca", "discount":0}
    ]


class Student():

    def __init__(self, code, name, academic_program_id, scholarship_id):   
        self._code = code
        self._name = name
        self._academic_program = ACADEMIC_PROGRAM_LIST[academic_program_id - 1]
        self._scholarship =  SCHOLARISHIP_LIST[scholarship_id - 1]
        self._total_cost = self.calculate_total_cost()

    def calculate_total_cost(self):
        return self._academic_program['cost'] - (self._academic_program['cost']*(self._scholarship['discount']/100))

        
    @property
    def code(self):
        return self._code

    def __str__(self):
        str = "| {:<10} | {:<40} | {:<30} | {:<20} | {:<30} | {:<20} | {:<20} |".format(self._code,self._name, self._academic_program['name'], self._academic_program['cost'], self._scholarship['type'], self._scholarship['discount'], self._total_cost)

        return str

def add_student():
    delete_screen()
    print('AGREGAR ESTUDIANTE\n\n')
    while(True):
        try:
            code = int(input('Código: '))
            break
        except ValueError:
            input('Debe ser un código numérico.')
    
    name = input('Nombre: ')

    while(True):
        print('Programas Académicos:')
        for program in ACADEMIC_PROGRAM_LIST:
            print(f"\t{program['id']}. {program['name']}")
        try:
            program_id = int(input())
            break
        except ValueError:
            input('No es una opción valida.')

    while(True):
        print('Becas Escolares')
        for scholarship in SCHOLARISHIP_LIST:
            print(f"{scholarship['id']}. {scholarship['type']}")
        try:
            scholarship_id = int(input())
            break
        except ValueError:
            input('No es una opción valida.')

    student = Student(code, name, program_id, scholarship_id)    
    STUDENTS.append(student)
    input('Ingreso Correcto.')

def show_student():
    
    if len(STUDENTS) == 0:
        input('No hay estudiantes registrados.')
    else:
        try:
            code = int(input('Ingrese el código del estudiante: '))
        except ValueError:
            input('Debe ser un código numérico.')
        delete_screen()
        header_table()
        for student in STUDENTS:
            if student.code == code:
                print(student)
                print('+','-'*188,'+')
        input()

def show_all_students():
    delete_screen()
    header_table()
    for student in STUDENTS:
        print(student)
        print('+','-'*188,'+')
    input()