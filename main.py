import students
from menu import main_menu


if __name__=='__main__':

    while(True):
        try:
            option=int(main_menu())
        except  ValueError:
            input ('Opción Invalida.')

        if option == 1:
            students.add_student()
        elif option == 2:
            students.show_student()
        elif option == 3:
            students.show_all_students()
        elif option == 0: 
            break 
        else:
            input ('Opción Invalida.')