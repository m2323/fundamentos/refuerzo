from delete_screen import delete_screen

def main_menu():
    delete_screen()
    option = int(input(
    """
    SISTEMA DE LIQUIDACIÓN DE MATRÍCULAS

    1. Agregar estudiante.
    2. Mostrar un estudiante
    3. Mostrar todos los estudiantes.
    0. Salir

    """
    ))
    return option

def header_table():
    print('+','-'*188,'+')
    print("| {:<10} | {:<40} | {:<30} | {:<20} | {:<30} | {:<20} | {:<20} |".format('Código','Nombre','Programa','Valor', 'Tipo de Beca', 'Porcentaje', 'Total'))
    print('+','-'*10,'+','-'*40,'+','-'*30,'+','-'*20,'+','-'*30,'+','-'*20,'+','-'*20,'+')