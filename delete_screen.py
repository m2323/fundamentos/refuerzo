import os

def delete_screen():
    '''
    Funcion para realizar una limpieza de pantalla.
    Realizar una verificación con os.name del sistema operativo.
    '''
    if os.name == "posix":
        os.system ("clear")
    elif os.name == "ce" or os.name == "nt" or os.name == "dos":
        os.system ("cls")
